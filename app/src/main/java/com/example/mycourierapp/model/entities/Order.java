package com.example.mycourierapp.model.entities;

public class Order {
    private int id;
    private String address;
    private String order;

    public Order(int id, String address, String order) {
        this.id = id;
        this.address = address;
        this.order = order;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getOrder() {
        return order;
    }
}
