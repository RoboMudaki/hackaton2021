package com.example.mycourierapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mycourierapp.model.entities.Order;
import com.example.mycourierapp.model.tool.DbHelper;

import java.util.ArrayList;
//1
public class TableOrders {

    private DbHelper dbHelper;

    public TableOrders(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public ArrayList<Order> GetOrders(){
        ArrayList<Order> orders = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `orders`";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            Order order = new Order(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2)
            );

            orders.add(order);
        }

        cursor.close();
        dbHelper.close();

        return orders;
    }



    public void DeleteById(int id){
        SQLiteDatabase db  = dbHelper.getWritableDatabase();

        String sqlCommand = "DELETE FROM `orders` WHERE `id` = '"+id+"'";

        db.execSQL(sqlCommand);

        dbHelper.close();
    }
}
