package com.example.mycourierapp.model.entities;

public class Courier {
    private int id;
    private String login;
    private String password;
    private String name;
    private String lastName;

    public Courier(int id, String login, String password, String name, String lastName) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
