package com.example.mycourierapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mycourierapp.model.entities.Courier;
import com.example.mycourierapp.model.tool.DbHelper;

public class TableCouriers {
    private DbHelper dbHelper;

    public TableCouriers(DbHelper dbHelper) {
        this.dbHelper = dbHelper;
    }



    public Courier GetByLoginAndPassword(String login, String password)
    {
        Courier courier = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE login='"+login+"' AND password='"+password+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            courier = new Courier(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }

        cursor.close();
        dbHelper.close();

        return courier;
    }

    public boolean ExistCourierByLogin(String login)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE login='"+login+"'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        boolean exist = cursor.moveToNext();

        cursor.close();
        dbHelper.close();

        return exist;
    }

    public Courier GetById(int courierId)
    {
        Courier courier = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `couriers` WHERE id="+courierId;

        Cursor cursor = db.rawQuery(sqlCommand,null);

        if(cursor.moveToNext()==true)
        {
            courier = new Courier(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }

        cursor.close();
        dbHelper.close();

        return courier;
    }
}
