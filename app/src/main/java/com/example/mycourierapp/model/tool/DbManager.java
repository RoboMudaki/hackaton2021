package com.example.mycourierapp.model.tool;

import android.content.Context;

import com.example.mycourierapp.model.tables.TableCouriers;
import com.example.mycourierapp.model.tables.TableOrders;

public class DbManager {

    private static DbManager instance = null;

    public static DbManager GetInstance(Context context)
    {
        if (instance==null)
        {
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableCouriers tableCouriers;
    private TableOrders tableOrders;

    private DbManager(Context context) {
        DbHelper dbHelper = new DbHelper(context);

        tableCouriers = new TableCouriers(dbHelper);
        tableOrders = new TableOrders(dbHelper);
    }

    public TableCouriers getTableCouriers() {
        return tableCouriers;
    }

    public TableOrders getTableOrders() {
        return tableOrders;
    }
}
