package com.example.mycourierapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mycourierapp.controller.ControllerMainActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ControllerMainActivity controller = new ControllerMainActivity(this);
        controller.InitializeFragments();
    }
}