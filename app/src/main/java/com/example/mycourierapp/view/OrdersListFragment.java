package com.example.mycourierapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerOrdersList;

public class OrdersListFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_orders_list, container, false);

        ControllerOrdersList controller =new ControllerOrdersList(view);
        controller.ShowAllOrders();

        return view;
    }
}
