package com.example.mycourierapp.view;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerOrderCompleting;
//112
public class CompletingOrderFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_completing_order, container, false);

        ControllerOrderCompleting controller = new ControllerOrderCompleting(view);
        controller.InitializeButtonsClick();

        ImageButton buttonSos = view.findViewById(R.id.imageButtonSos);
        buttonSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+1776909));
                startActivity(callIntent);
            }
        });

        return view;
    }
}
