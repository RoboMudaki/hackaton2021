package com.example.mycourierapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerCourierInfo;

public class CourierInfoFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_courier_info, container, false);

        ControllerCourierInfo controller = new ControllerCourierInfo(view);
        controller.InitializeFragment();
        controller.InitializeButtonsClick();

        return view;
    }
}
