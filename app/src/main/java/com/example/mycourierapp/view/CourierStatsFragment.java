package com.example.mycourierapp.view;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerCourierInfo;
import com.example.mycourierapp.controller.ControllerCourierStats;

public class CourierStatsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_courier_stats, container, false);

        ControllerCourierStats controller = new ControllerCourierStats(view);
        controller.InitializeButton();

        return view;
    }
}
