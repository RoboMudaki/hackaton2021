package com.example.mycourierapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.service.controls.Control;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerLoginPage;
import com.example.mycourierapp.controller.ControllerTalkWithManager;

public class TalkWithManagerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragmetn_talk_with_manager, container, false);

        ControllerTalkWithManager controller = new ControllerTalkWithManager(view);
        controller.InitializeButtons();

        return view;
    }
}
