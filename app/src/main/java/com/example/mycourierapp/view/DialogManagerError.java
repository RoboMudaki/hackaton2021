package com.example.mycourierapp.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;



import com.example.mycourierapp.model.tool.DataStorage;

public class DialogManagerError extends AlertDialog {

    public DialogManagerError(Context context) {
        super(context);
    }

    @NonNull
    public Dialog onCreateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder((Context) DataStorage.Get("context"));
        builder.setTitle("Ошибка отправки сообщения")
                .setMessage("Проверте подключение к интернету")
                .setPositiveButton("ок", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}
