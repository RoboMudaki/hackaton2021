package com.example.mycourierapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mycourierapp.R;
import com.example.mycourierapp.controller.ControllerEndOder;
import com.example.mycourierapp.controller.ControllerLoginPage;

public class EndOrderFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_end_order, container, false);

        ControllerEndOder controller = new ControllerEndOder(view);
        controller.InitializeButton();

        return view;
    }
}
