package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.entities.Courier;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.CourierStatsFragment;
import com.example.mycourierapp.view.OrdersListFragment;

public class ControllerCourierInfo {
    private View view;
    private DbManager db;


    public ControllerCourierInfo(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeFragment(){
        Courier courier = (Courier) DataStorage.Get("courier");

        TextView textViewLogin = view.findViewById(R.id.textViewLogin);
        TextView textViewName = view.findViewById(R.id.textViewName);
        TextView textViewLastName = view.findViewById(R.id.textViewLastName);

        textViewLogin.setText("Логин: "+courier.getLogin());
        textViewName.setText("Имя: "+courier.getName());
        textViewLastName.setText("Фамилия: "+courier.getLastName());
    }

    public void InitializeButtonsClick(){
        Button buttonShowOrders = view.findViewById(R.id.buttonShowOrders);
        buttonShowOrders.setOnClickListener(ButtonShowOrdersOnClick);
        Button buttonShowStats = view.findViewById(R.id.buttonShowStats);
        buttonShowStats.setOnClickListener(ButtonShowStats);
    }

    private View.OnClickListener ButtonShowOrdersOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OrdersListFragment ordersListFragment = new OrdersListFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, ordersListFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonShowStats = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CourierStatsFragment courierStatsFragment = new CourierStatsFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, courierStatsFragment);
            fragmentTransaction.commit();
        }
    };
}
