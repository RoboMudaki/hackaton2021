package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.CourierInfoFragment;

public class ControllerEndOder {
    View view;

    public ControllerEndOder(View view) {
        this.view = view;
    }

    public void InitializeButton(){
        Button buttonBackToHome = view.findViewById(R.id.buttonBackToHome);
        buttonBackToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

                MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

                FragmentTransaction fragmentTransaction;
                fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
                fragmentTransaction.commit();
            }
        });
    }
}
