package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.entities.Order;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.view.CompletingOrderFragment;

import java.util.ArrayList;

public class RvAdapterOrdersList extends RecyclerView.Adapter<RvAdapterOrdersList.OrderViewHolder> {

    ArrayList<Order> orders;

    public RvAdapterOrdersList(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public static class OrderViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewAddress;
        public TextView textViewOrder;

        public Button buttonTakeOrder;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewOrder = itemView.findViewById(R.id.textViewOrder);
            buttonTakeOrder = itemView.findViewById(R.id.buttonTakeOrder);
        }
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_item_order,viewGroup,false);
        OrderViewHolder ovh = new OrderViewHolder(v);
        return ovh;
    }



    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int i) {
        holder.textViewAddress.setText(orders.get(i).getAddress());
        holder.textViewOrder.setText(orders.get(i).getOrder());

        holder.buttonTakeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DataStorage.Add("CurrentOrderId",orders.get(i).getId());

                MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

                CompletingOrderFragment completingOrderFragment = new CompletingOrderFragment();

                FragmentTransaction fragmentTransaction;
                fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragmentsContainerMain, completingOrderFragment);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
