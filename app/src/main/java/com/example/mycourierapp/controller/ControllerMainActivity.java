package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.CourierInfoFragment;
import com.example.mycourierapp.view.LoginPageFragment;

public class ControllerMainActivity {
    private MainActivity mainActivity;

    private DbManager db;

    private LoginPageFragment loginPageFragment;
    private CourierInfoFragment courierInfoFragment;

    public ControllerMainActivity(MainActivity mainActivity) {
        this.mainActivity =  mainActivity;
        DataStorage.Add("mainActivity", mainActivity);
        db = DbManager.GetInstance(this.mainActivity.getApplicationContext());
    }

    public void InitializeFragments(){
        loginPageFragment = new LoginPageFragment();
        courierInfoFragment = new CourierInfoFragment();

        FragmentTransaction fragmentTransaction;
        fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.fragmentsContainerMain,loginPageFragment);

        fragmentTransaction.commit();
    }
}
