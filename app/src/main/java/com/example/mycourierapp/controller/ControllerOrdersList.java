package com.example.mycourierapp.controller;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mycourierapp.R;
import com.example.mycourierapp.model.entities.Order;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;

import java.util.ArrayList;

public class ControllerOrdersList {
    View view;
    DbManager db;


    public ControllerOrdersList(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void ShowAllOrders(){
        ArrayList<Order> orders = db.getTableOrders().GetOrders();

        Context context = (Context) DataStorage.Get("context");

        RecyclerView recyclerViewCatalog = view.findViewById(R.id.recyclerViewOrders);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        recyclerViewCatalog.setLayoutManager(llm);

        RvAdapterOrdersList adapter = new RvAdapterOrdersList(orders);
        recyclerViewCatalog.setAdapter(adapter);
    }
}
