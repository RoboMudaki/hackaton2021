package com.example.mycourierapp.controller;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.core.app.ActivityCompat;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.entities.Courier;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.EndOrderFragment;
import com.example.mycourierapp.view.TalkWithManagerFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

public class ControllerOrderCompleting {

    View view;
    DbManager db;
    Context context;

    public ControllerOrderCompleting(View view) {
        this.view = view;
        context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);

    }

    public void InitializeButtonsClick(){
        ImageButton buttonCallManager = view.findViewById(R.id.imageButtonTalkWirhManager);
        Button buttonEndOrder = view.findViewById(R.id.buttonEndOrder);

        buttonEndOrder.setOnClickListener(ButtonEndOrderOnClick);
        buttonCallManager.setOnClickListener(ButtonCallManagerOnClick);
    }

    private View.OnClickListener ButtonCallManagerOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TalkWithManagerFragment talkWithManagerFragment = new TalkWithManagerFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, talkWithManagerFragment);
            fragmentTransaction.commit();
        }
    };

    private View.OnClickListener ButtonEndOrderOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = (Integer) DataStorage.Get("CurrentOrderId");

            db.getTableOrders().DeleteById(id);
            DataStorage.Delete("CurrentOrderId");

            EndOrderFragment endOrderFragment = new EndOrderFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, endOrderFragment);
            fragmentTransaction.commit();
        }
    };





}
