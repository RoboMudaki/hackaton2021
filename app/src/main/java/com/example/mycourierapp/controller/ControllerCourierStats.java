package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.CourierInfoFragment;
import com.example.mycourierapp.view.CourierStatsFragment;

public class ControllerCourierStats {

    private View view;
    private DbManager db;

    public ControllerCourierStats(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButton(){
        Button buttonBack = view.findViewById(R.id.buttonBackToCoruer);
        buttonBack.setOnClickListener(ButtonBackOnClick);
    }

    private View.OnClickListener ButtonBackOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

            MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");

            FragmentTransaction fragmentTransaction;
            fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
            fragmentTransaction.commit();
        }
    };
}
