package com.example.mycourierapp.controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mycourierapp.MainActivity;
import com.example.mycourierapp.R;
import com.example.mycourierapp.model.entities.Courier;
import com.example.mycourierapp.model.tool.DataStorage;
import com.example.mycourierapp.model.tool.DbManager;
import com.example.mycourierapp.view.CourierInfoFragment;

public class ControllerLoginPage {
    private View view;
    private DbManager db;


    public ControllerLoginPage(View view) {
        this.view = view;
        Context context = (Context) DataStorage.Get("context");
        db = DbManager.GetInstance(context);
    }

    public void InitializeButtonsClick(){
        Button buttonLogin = view.findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(OnButtonLoginClickListener);
    }

    private void AuthorizeCourier(){
        EditText editTextLogin = view.findViewById(R.id.editTextLogin);
        EditText editTextPassword = view.findViewById(R.id.editTextPassword);

        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        Context context = (Context) DataStorage.Get("context");

        if (db.getTableCouriers().ExistCourierByLogin(login)){
            Courier courier = db.getTableCouriers().GetByLoginAndPassword(login, password);

            if (courier != null) {
                CourierInfoFragment courierInfoFragment = new CourierInfoFragment();

                MainActivity mainActivity = (MainActivity) DataStorage.Get("mainActivity");
                DataStorage.Add("courier", courier);

                FragmentTransaction fragmentTransaction;
                fragmentTransaction = mainActivity.getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragmentsContainerMain, courierInfoFragment);
                fragmentTransaction.commit();
            } else {
                Toast.makeText(context, "Неверный логин или пароль", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(context, "Неверный логин или пароль", Toast.LENGTH_LONG).show();
        }


    }

    private View.OnClickListener OnButtonLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AuthorizeCourier();
        }
    };
}
